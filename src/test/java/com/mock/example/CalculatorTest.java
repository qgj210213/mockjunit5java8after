package com.mock.example;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

/**
 * @author ts-guangjie.qi
 * junit5 test samples
 *
 */
public class CalculatorTest {
    @Test
    @DisplayName("1 + 1 = 2")
    public void addsTwoNumbers() {
        Calculator calculator = new Calculator();
        assertEquals(2, calculator.add(1, 1), "1 + 1 should equal 2");
    }

    /**
     * @CsvSource允许你将参数列表定义为以逗号分隔的值（即String类型的值）。
     * @param first
     * @param second
     * @param expectedResult
     */
    @ParameterizedTest(name = "{0} +{1} = {2}")
    @CsvSource({
            "0,1,1",
            "1,2,3",
            "49,51,100",
            "1,100,101"
    })
    public void add(int first, int second, int expectedResult) {
        Calculator calculator = new Calculator();
        assertEquals(expectedResult, calculator.add(first, second),
                () -> first + " + " + second + "should equal " + expectedResult);
    }

}
