package com.mock.chapter01;

import static org.mockito.Mockito.*;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @Mock
    private List<String> users;
    @InjectMocks
    private UserService userService;
    @Test
    @DisplayName("Should add a user to list")
    public void shouldAddUserToList() {
        userService.addUser("hello");
        verify(users, times(1)).add("hello");
    }

}
