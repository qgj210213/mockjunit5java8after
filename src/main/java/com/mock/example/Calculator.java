package com.mock.example;

/**
 * @author ts-guangjie.qi
 *简单的测试类
 *http://www.eclipse.org/legal/epl-v20.html
 */
public class Calculator {
    public int add(int a, int b) {
        return a + b;
    }

}
